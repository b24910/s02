
//student one
// let studentOneName = 'John'
// let studentOneEmail = 'john@mail.com'
// let studentOneGrades = [89, 83, 78, 88]


//student two
// let studentTwoName = 'Joe'
// let studentTwoEmail = 'joe@mail.com'
// let studentTwoGrades = [89, 82, 78, 85]


//student three
// let studentThreeName = 'Jane'
// let studentThreeEmail = 'jane@mail.com'
// let studentThreeGrades = [87, 89, 78, 93]


// student four
// let studentFourName = 'Jessie'
// let studentFourEmail = 'jessie@mail.com'
// let studentFourGrades = [91, 89, 92, 93]


// function login(email){
//     console.log(`${email} has logged in`)
// }

// function logout(email){
//     console.log(`${email} has logged out`)
// }

// function list(grades){
//     grades.forEach(grade => {
//         console.log(grade)
//     })
// }


let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    computeAve () {
        const ave = this.grades.reduce((a, b) => a + b, 0) /  this.grades.length
        return ave
    },

    willPass(){
        return (this.computeAve()) >= 85 ? true : false;
    },

    willPassWithHonnors (){
        return this.willPass() && this.computeAve() >= 90;
    }
}
console.log(studentOne.willPassWithHonnors())

// function computeAve (val) {
//     const ave = val.reduce((a, b) => a + b, 0) / val.length
//     return ave
// }

// console.log(computeAve(studentOne.grades))

// quiz 02


// 1
"spaghetti code"

// 2
"key value pairs and methods wrapped in curly braces"

// 3
"Object Oriented Programming"

// 4
"studentOne.enroll()"

// 5
"true"

// 6
"const obj = { Key: value }"

// 7
"true"

// 8
"true"

// 9
"true"

// 10
"true"


// function Coding


let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    computeAve () {
        const ave = this.grades.reduce((a, b) => a + b, 0) /  this.grades.length
        return ave
    },

    willPass(){
        return (this.computeAve()) >= 85 ? true : false;
    },

    willPassWithHonnors (){
        return this.willPass() && this.computeAve() >= 90;
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    computeAve () {
        const ave = this.grades.reduce((a, b) => a + b, 0) /  this.grades.length
        return ave
    },

    willPass(){
        return (this.computeAve()) >= 85 ? true : false;
    },

    willPassWithHonnors (){
        return this.willPass() && this.computeAve() >= 90;
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    computeAve () {
        const ave = this.grades.reduce((a, b) => a + b, 0) /  this.grades.length
        return ave
    },

    willPass(){
        return (this.computeAve()) >= 85 ? true : false;
    },

    willPassWithHonnors (){
        return this.willPass() && this.computeAve() >= 90;
    }
}

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents(){
        arr = []
        this.students.forEach((student)=>{
            if(student.willPassWithHonnors()){
                arr.push(student)
            }
        })
        return arr.length;
    },
    honersPercentage(){
        const calc = (this.countHonorStudents()/this.students.length) * 100 ;
        return `${calc}%`
    },

    retrieveHonorStudentInfo(){
        arr = []
        newArr =[]
        this.students.forEach((student)=>{
            if(student.willPassWithHonnors()){
                arr.push(student)
            }
        })
        
        arr.map((student)=>{
            newArr.push({
                aveGrade: student.grades.reduce((a, b) => a + b, 0) /  student.grades.length,
                email: student.email
            })
        })
        return newArr;
    },

    sortHonorStudentsByGradesDec(){
        return this.retrieveHonorStudentInfo().sort(function(a, b){
            let x = a.aveGrade
            let y = b.aveGrade
            if (x < y) {return 1;}
            if (x > y) {return -1;}
            return 0
        })
        
    }
}



console.log(classOf1A.honersPercentage())

console.log(classOf1A.countHonorStudents())

console.log(classOf1A.students)

console.log(classOf1A.sortHonorStudentsByGradesDec())